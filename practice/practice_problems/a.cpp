#include <stdio.h>
#include <string.h>
#include <iostream>

using namespace std;

char alien[32];
char source[95];
char target[95];

int main()
{
    int T;
    int a_len, bs, bt;
    int result;

    freopen("data.in", "r", stdin);
    freopen("data.out", "w", stdout);

    cin >> T;
    for(int i=1; i<=T; i++) {
        cin >> alien;
        cin >> source;
        cin >> target;

        a_len = strlen(alien);
        bs = strlen(source);
        bt = strlen(target);

        result = 0;
        for(int j=0; j<a_len; j++) {
            for(int k=0; k<bs; k++) {
                if(source[k] == alien[j]) {
                    result *= bs;
                    result += k;
                }
            }
        }

        int remain;
        int j = 0;
        while(result > 0)
        {
            remain = result % bt;
            result /= bt;

            alien[j++] = target[remain];
        }

        cout << "Case #" << i << ": ";
        for(int i=j-1; i>=0; i--)
            cout << alien[i];
        cout << endl;
    }

    return 0;
}
